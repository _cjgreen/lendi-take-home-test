/**
 * Trigger to handle any processing to a Contact
 */
trigger ContactTrigger on Contact(after insert, after update) {
    // Delegate logic processing to ContactTriggerHandler After Insert handler
    if (Trigger.isAfter && Trigger.isInsert) {
        ContactTriggerHandler.handleAfterInsert(Trigger.new);
    }

    // Delegate logic processing to ContactTriggerHandler After Update handler
    if (Trigger.isAfter && Trigger.isUpdate) {
        ContactTriggerHandler.handleAfterUpdate(Trigger.new, Trigger.oldMap);
    }
}