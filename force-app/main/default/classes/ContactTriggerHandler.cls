/**
 * Class to handle any processing required from the Contact Trigger
 */
public class ContactTriggerHandler {
    /**
     * Function to handle any processing required to a Contact After Insert
     */
    public static void handleAfterInsert(List<Contact> newContacts) {
        // List of new Contacts who have set their FirstName or LastName
        List<Contact> contactNameCheckList = new List<Contact>();

        for (Contact newContact : newContacts) {
            // Check if new Contact FirstName or LastName is set
            if (newContact.FirstName != null || newContact.LastName != null) {
                contactNameCheckList.add(newContact);
            }
        }

        // If any new Contacts have set their FirstName or LastName pass them to
        // LendiTakeHomeTest.checkName() to check their name
        if (contactNameCheckList.size() > 0) {
            LendiTakeHomeTest.checkName(contactNameCheckList);
        }
    }

    /**
     * Function to handle any processing required to a Contact After Update
     */
    public static void handleAfterUpdate(List<Contact> newContacts, Map<Id, Contact> oldContacts) {
        // List of updated Contacts who have changed their FirstName or LastName
        List<Contact> contactNameCheckList = new List<Contact>();

        for (Contact newContact : newContacts) {
            Contact oldContact = oldContacts.get(newContact.Id);

            // Check if updated Contact FirstName or LastName has changed to anything other than blank
            if ((newContact.FirstName != oldContact.FirstName && newContact.FirstName != null) ||
                 newContact.LastName != oldContact.LastName && newContact.LastName != null) {
                contactNameCheckList.add(newContact);
            }
        }

        // If any new Contacts have changed their FirstName or LastName pass them to
        // LendiTakeHomeTest.checkName() to check their updated name
        if (contactNameCheckList.size() > 0) {
            LendiTakeHomeTest.checkName(contactNameCheckList);
        }
    }
}
