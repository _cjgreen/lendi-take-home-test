/**
 * HttpCalloutMock Class to simulate the successful and failed responses from the Mailgun API
 */
@isTest
public class MailgunAPIServiceMock implements HttpCalloutMock {
    protected String responseJSON;
    protected Integer responseCode;
    
    /**
     * Function to initialise the MailgunAPIServiceMock as a success or failed response
     */
    public MailgunAPIServiceMock(Boolean res){
        if (res) {
            // If MailgunAPIServiceMock has been initilised with res as true, set the responseJSON and responseCode
            // to successful Mailgun API responses to simulate a successful response
            responseJSON = '{ "id": "<20200521115045.1.93CBDE3A3C9A77EC@mg.cjgreen.com.au>", "message": "Queued. Thank you." }';
            responseCode = 200;
        } else {
            // If MailgunAPIServiceMock has been initilised with res as false, set the responseJSON and responseCode
            // to failed Mailgun API responses to simulate a successful response
            responseJSON = '{ "message": "\'from\' parameter is missing" }';
            responseCode = 400;
        }
    }
    
    /**
     * Function to respond to any HTTP Callouts after the MailgunAPIServiceMock has been initialised
     * The function responds with the responseJSON and responseCode as initialised
     */
    public HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody(responseJSON);
        response.setStatusCode(responseCode);
        return response;
    }
}