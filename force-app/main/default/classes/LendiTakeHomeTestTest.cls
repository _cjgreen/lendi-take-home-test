/**
 * Test Class to Unit Test the functionality of the LendiTakeHomeTest:
 * - Check if the name of a Contact matches the value specified in
 *   Lendi_Take_Home_Test_Configuration__c Custom Setting and them email
 *   them using the email configuration defined in the
 *   Lendi_Take_Home_Test_Configuration__c Custom Setting via the Mailgun API
 */
@isTest
public with sharing class LendiTakeHomeTestTest {
    /**
     * Function to define any common test data that is needed for any of the test in the class
     */
    @testSetup
    public static void createTestData() {
        // Define the Lendi_Take_Home_Test_Configuration__c Custom Setting used
        // for Contact Name Trigger Value check and Email Configuration
        Lendi_Take_Home_Test_Configuration__c lendiTakeHomeTestConfiguration = new Lendi_Take_Home_Test_Configuration__c();
        lendiTakeHomeTestConfiguration.Contact_Name_Trigger_Value__c = 'Lendi';
        lendiTakeHomeTestConfiguration.Email_From__c = 'Excited User <mailgun@YOUR_DOMAIN_NAME>';
        lendiTakeHomeTestConfiguration.Email_Subject__c = 'Hello';
        lendiTakeHomeTestConfiguration.Email_Body__c = 'Testing some Mailgun awesomeness!';
        insert lendiTakeHomeTestConfiguration;
    }
    
    /**
     * Function to test if when a new Contact is created and the name matches the 
     * Lendi_Take_Home_Test_Configuration__c.Contact_Name_Trigger_Value__c Custom Setting Value,
     * a successful email task is attached to the Contact
     */
    @isTest
    public static void newNameMatchSuccess() {
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MailgunAPIServiceMock(true));
            Contact contact = new Contact();
            contact.LastName = 'Lendi';
            contact.Email = 'lendi@cjgreen.com.au';
            insert contact;
        Test.stopTest();

        List<Task> tasks = [SELECT Subject FROM Task WHERE WhoID = :contact.Id];

        System.assert(tasks.size() > 0, 'No email task created.');
        System.assert(tasks[0].Subject.endsWith(': Success'), 'Email task subject does not end with : Success.');
    }

    /**
     * Function to test if when a Contact Name is updated and the name matches the 
     * Lendi_Take_Home_Test_Configuration__c.Contact_Name_Trigger_Value__c Custom Setting Value,
     * a successful email task is attached to the Contact
     */
    @isTest
    public static void updateNameMatchSuccess() {
        Contact contact = new Contact();
        contact.LastName = 'Lender';
        contact.Email = 'lendi@cjgreen.com.au';
        insert contact;

        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MailgunAPIServiceMock(true));
            contact.LastName = 'Lendi';
            update contact;
        Test.stopTest();

        List<Task> tasks = [SELECT Subject FROM Task WHERE WhoID = :contact.Id];

        System.assert(tasks.size() > 0, 'No email task created.');
        System.assert(tasks[0].Subject.endsWith(': Success'), 'Email task subject does not end with : Success.');
    }

    /**
     * Function to test if when a new Contact is created and the name matches the 
     * Lendi_Take_Home_Test_Configuration__c.Contact_Name_Trigger_Value__c Custom Setting Value,
     * and the MailgunAPIService returns a failed response, a Fail email task is attached to the Contact
     */
    @isTest
    public static void emailSendFailure() {
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MailgunAPIServiceMock(false));
            Contact contact = new Contact();
            contact.LastName = 'Lendi';
            contact.Email = 'lendi@cjgreen.com.au';
            insert contact;
        Test.stopTest();

        List<Task> tasks = [SELECT Subject FROM Task WHERE WhoID = :contact.Id];

        System.assert(tasks.size() > 0, 'No email task created.');
        System.assert(tasks[0].Subject.endsWith(': Fail'), 'Email task subject does not end with : Fail.');
    }
}
