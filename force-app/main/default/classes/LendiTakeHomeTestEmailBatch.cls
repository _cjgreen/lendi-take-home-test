/**
 * Batch job to send an email to any contacts defined using the email configuration
 * defined in the Lendi_Take_Home_Test_Configuration__c Custom Setting
 */
public class LendiTakeHomeTestEmailBatch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {

    // Define query as a public variable to allow a query override when running
    // through Developer Console on demand
    public String query;

    // Define a Set of Id's that can be populated before execution to specify
    // which contacts to query and send emails to
    public Set<Id> contactIds;

    /**
     * Function that is run before any of the batch jobs execute to initilise any variables
     * and define the query to be batched
     */
    public Database.QueryLocator start(Database.BatchableContext bc) {
        // Return gracefully and log an error if Lendi_Take_Home_Test_Configuration__c Custom Setting can not be loaded
        if (LendiTakeHomeTest.lendiTakeHomeTestConfiguration == null) {
            System.debug(Logginglevel.ERROR, 'Could not get LendiTakeHomeTest.lendiTakeHomeTestConfiguration.');
            return null;
        }

        // If no query override has been specified used the default query
        if (query == null || query == '') {
            query = 'SELECT Id, Email FROM Contact WHERE Email != null';
        }

        // If contact Id have been specified add them to the query
        if (contactIds != null && contactIds.size() > 0) {
            query += ' AND Id IN :contactIds';
        }

        return Database.getQueryLocator(query);	
    }
    
    /**
     * Function executed for each chunk of the batch, with scope size no greater than the
     * batch size specified in the execute function
     */
    public void execute(Database.BatchableContext bc, List<Contact> scope) {
        // List of email tasks to be inserted to record success and failure of email sends on the contact
        List<Task> tasks = new List<Task>();
        
        for (Contact contact : scope) {

            // Construct a Mailgun email using the From, Subject and Text specified in the 
            // Lendi_Take_Home_Test_Configuration__c Custom Setting
            MailgunAPIService.Email email = new MailgunAPIService.Email();
            email.fromField = LendiTakeHomeTest.lendiTakeHomeTestConfiguration.Email_From__c;
            email.toField = contact.Email;
            email.subject = LendiTakeHomeTest.lendiTakeHomeTestConfiguration.Email_Subject__c;
            email.text = LendiTakeHomeTest.lendiTakeHomeTestConfiguration.Email_Body__c;

            // Send the Email using the MailgunAPIService
            MailgunAPIService.Response emailResponse = MailgunAPIService.sendEmail(email);

            // Create an Email Task linked to the Contact to record Success or Failure of the Email Send
            // and the Email contents
            Task task = new Task();
            task.Description = email.text;

            if (emailResponse != null) {
                // If Email Send was succesful add ': Success' to the task subject
                task.Subject = email.subject + ': Success';

                // Add the Mailgun response to the task description for verification purposes
                task.Description += '\r\n************ Mailgun Response ************\r\n';
                task.Description += 'id: ' + emailResponse.id;
                task.Description += 'message: ' + emailResponse.message;
            } else {
                // If Email Send was NOT succesful add ': Fail' to the task subject
                task.Subject = email.subject + ': Fail';
            }

            task.TaskSubtype = 'Email';
            task.ActivityDate = Date.today();
            task.WhoID = contact.Id;
            task.Status = 'Completed';
            tasks.add(task);
        }


        // If any email tasks were created, insert them
        if (tasks.size() > 0) {
            insert tasks;
        }
    }

    /**
     * Function executed after all batches are completed executing
     */
    public void finish(Database.BatchableContext bc) {
        // Log the information from the batch execution for informational purposes
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id = :bc.getJobId()];
        System.debug(Logginglevel.INFO, 'LendiTakeHomeTestEmailBatch Complete: ' + job);
    }
}