/**
 * Class to send a Mailgun API Email using the Mailgun API
 */
public class MailgunAPIService {
    // Mailgun API Email definition
    public class Email {
        public String fromField {get; set;}
        public String toField {get; set;}
        public String subject {get; set;}
        public String text {get; set;}
    }

    // Mailgun API Response definition
    public class Response {
        public String id {get; set;}
        public String message {get; set;}
    }

    /**
     * Function to send a Mailgun API Email using the Mailgun API
     */
    public static Response sendEmail(Email email) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();

        // Set the http request endpoint to the Mailgun API Named Credential
        // This will automatically add the Basic Auth headers to the request 
        request.setEndpoint('callout:Mailgun_API');
        request.setMethod('POST');
        
        // Set the body of the http request using multipart/form-data
        // This needs to be manually built as Salesforce HttpRequest does not support this natively
        String boundary = '----------------------------' + String.valueOf(DateTime.now().getTime());
        String body = '--' + boundary + '\r\n';
        body += 'Content-Disposition: form-data; name="from"\r\n\n';
        body += email.fromField + '\r\n';
        body += '--' + boundary + '\r\n';
        body += 'Content-Disposition: form-data; name="to"\r\n\n';
        body += email.toField + '\r\n';
        body += '--' + boundary + '\r\n';
        body += 'Content-Disposition: form-data; name="subject"\r\n\n';
        body += email.subject + '\r\n';
        body += '--' + boundary + '\r\n';
        body += 'Content-Disposition: form-data; name="text"\r\n\n';
        body += email.text + '\r\n';
        body += '--' + boundary + '--';
        request.setBody(body);

        request.setHeader('Content-Type', 'multipart/form-data; boundary=' + boundary);
        request.setHeader('Content-Length', String.valueof(body.length()));

        try {
            // Send the http request
            response = http.send(request);

            // Check is the http request was successful, if so map the response body
            // to a Mailgun API Response definition and return it
            if (response.getStatusCode() == 200) {
                return (Response) JSON.deserialize(response.getBody(), Response.class);
            } else {
                // Log an error if the http response was not successfull and return null
                System.debug(LoggingLevel.ERROR, 'Failed to send request to Mailgun: ' + response.getBody());
                return null;
            }
        } catch (Exception error) {
            // Log an error if an Exception occured return null
            System.debug(LoggingLevel.ERROR, 'Failed to send request to Mailgun: ' + error.getMessage());
            return null;
        }
    }
}
