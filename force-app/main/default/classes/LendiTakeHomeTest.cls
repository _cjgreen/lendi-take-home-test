/**
 * Class to check if the name of a Contact matches the value specified in
 * Lendi_Take_Home_Test_Configuration__c Custom Setting and them email them.
 */
public class LendiTakeHomeTest {
    /**
     * Function to lazy load Lendi_Take_Home_Test_Configuration__c Custom Setting
     */
    public static Lendi_Take_Home_Test_Configuration__c lendiTakeHomeTestConfiguration {
        get {
            if (lendiTakeHomeTestConfiguration == null) {
                List<Lendi_Take_Home_Test_Configuration__c> lendiTakeHomeTestConfigurationList = [SELECT Contact_Name_Trigger_Value__c, Email_From__c, Email_Subject__c, Email_Body__c FROM Lendi_Take_Home_Test_Configuration__c];

                if (lendiTakeHomeTestConfigurationList.size() > 0) {
                    lendiTakeHomeTestConfiguration = lendiTakeHomeTestConfigurationList[0];
                }
            }
            return lendiTakeHomeTestConfiguration;
        }
        private set;
    }

    /**
     * Function to check if the name of a Contact matches the value specified in
     * Lendi_Take_Home_Test_Configuration__c Custom Setting
     */
    public static void checkName(List<Contact> contacts) {
        // Return gracefully and log an error if Lendi_Take_Home_Test_Configuration__c Custom Setting can not be loaded
        if (LendiTakeHomeTest.lendiTakeHomeTestConfiguration == null) {
            System.debug(Logginglevel.ERROR, 'Could not get LendiTakeHomeTest.lendiTakeHomeTestConfiguration.');
            return;
        }

        // Retreive value to match with Contact name from Lendi_Take_Home_Test_Configuration__c Custom Setting
        String contactNameTriggerValue = LendiTakeHomeTest.lendiTakeHomeTestConfiguration.Contact_Name_Trigger_Value__c;

        // Set of Contact Id's whos FirstName or LastName matched the contactNameTriggerValue
        Set<Id> matchedContactIds =  new Set<Id>();

        for (Contact contact : contacts) {
            // Check if Contacts FirstName or LastName matches the contactNameTriggerValue
            if (contact.FirstName == contactNameTriggerValue || contact.LastName == contactNameTriggerValue) {
                matchedContactIds.add(contact.Id);
            }
        }

        // If any Contacts FirstName or LastName matched the contactNameTriggerValue pass their Id's
        // to the LendiTakeHomeTestEmailBatch to send them an Email
        if (matchedContactIds.size() > 0) {
            LendiTakeHomeTestEmailBatch lendiTakeHomeTestEmailBatch = new LendiTakeHomeTestEmailBatch();
            lendiTakeHomeTestEmailBatch.contactIds = matchedContactIds;

            // Batch size set to 10 to ensure max callout limit of 10 is not exceeded 
            Database.executeBatch(lendiTakeHomeTestEmailBatch, 10);
        }
    }
}
