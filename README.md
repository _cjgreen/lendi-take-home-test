## Lendi Take-Home Test

#### Technical Method: Trigger with API Call

Please implement the below requirement in your own Dev org:

**Requirements:** On Contact, if the name is matching to a specific criteria, use mailgun to send
the email to the current contact

Notes:

1. You can define your own criteria
2. Use your own developer org
3. Test code coverage should be above 85%
4. Exception handling strategy should be user friendly

## Development, Deployment and Release Prerequisites

* [Salesforce with Dev Hub enabled](https://developer.salesforce.com/docs/atlas.en-us.sfdx_setup.meta/sfdx_setup/sfdx_setup_enable_devhub.htm)
* [Unlocked Packages enabled in your Dev Hub](https://developer.salesforce.com/docs/atlas.en-us.sfdx_setup.meta/sfdx_setup/sfdx_setup_enable_secondgen_pkg.htm)
* [Salesforce CLI](https://developer.salesforce.com/tools/sfdxcli)
* [VSCode with Salesforce Extensions](https://developer.salesforce.com/tools/vscode) *Optional*

## Development Instructions

If your Dev Hub is not connected, log in to it:
`sfdx force:auth:web:login -d -a DevHub`

Create a scratch org, in which to install the unlocked package, with the alias MyScratchOrg.
`sfdx force:org:create -s -f config/project-scratch-def.json -a MyScratchOrg -v DevHub`

Push code to scratch org.
`sfdx force:source:push`

Open scratch org and follow 'Post Installation Instructions' to configure environment
`sfdx force:org:open`

Make any required code changes.
`code .`

Push code changes to scratch org.
`sfdx force:source:push`

Make any required config changes.
`sfdx force:org:open`

Pull config changes from scratch org. *Discard changes to Named Credentials*
`sfdx force:source:pull`

If you changes are Major, Minor or Patch, increment `packageDirectories.versionName` and `packageDirectories.versionNumber` as required in `sfdx-project.json`.

## Deployment Instructions

If you havn't created an unlocked package using your Dev Hub for this project yet, created an unlocked package without a namespace, and supply the alias or username for your Dev Hub org if it’s not already set as the default:
`sfdx force:package:create --name lendi-take-home-test --description "Lendi Take-Home Test" --packagetype Unlocked --path force-app --nonamespace --targetdevhubusername DevHub`

Create the package version, which associates the metadata with the package.
`sfdx force:package:version:create -p lendi-take-home-test -d force-app -k rOcrrffMqUG% --wait 10 -v DevHub`

Use the new package version alias in `sfdx-project.json` to install the package version in the scratch org that you created earlier.
`sfdx force:package:install --wait 10 --publishwait 10 --package lendi-take-home-test@X.X.X-X -k rOcrrffMqUG% -r -u MyScratchOrg`

## Release Instructions

When you know that a version is ready for release, you can promote the package version to released.
`sfdx force:package:version:promote -p lendi-take-home-test@X.X.X-X -v DevHub`

To install the package version in to another Org e.g. Production, log in to it.
`sfdx force:auth:web:login -a Production`

Install the package version in to the newly logged in org using its alias e.g Production.
`sfdx force:package:install --wait 10 --publishwait 10 --package lendi-take-home-test@X.X.X-X -k rOcrrffMqUG% -r -u Production`

Open the org you installed the package in e.g. Production and follow 'Post Installation Instructions' to configure environment
`sfdx force:org:open -u Production`

## Post Installation Instructions

Update 'Mailgun API' Named Credential with your own URL (Domain) and Password (API Key)
`Setup->Security->Named Credentials->Mailgun API->Edit`

Create new 'Lendi Take-Home Test Configuration' Custom Setting record, update the default values as desired and add your verified Mailgun domain to the Email From.
`Setup->Custom Code->Custom Settings->Lendi Take-Home Test Configuration->Manage->New`